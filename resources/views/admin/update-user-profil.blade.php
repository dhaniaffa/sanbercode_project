@extends('layouts.admin-lte.master')
@section('content')
    <div class="card">
        <div class="card-header bg-dark">
            <b>Profil {{Auth::user()->name}}</b>
        </div>
        <div class="card-body">
            <form action="{{route('profil.update',['id'=>Auth::id()])}}" method="post" class="w-50">
                @csrf
                @method('PUT')
                <div class="form-group">
                    <label for="">Nama</label>
                    <input type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{old('name')??Auth::user()->name}}">
                    @error('name') <span class="text-danger">{{$message}}</span> @enderror
                </div>
                <div class="form-group">
                    <label for="">E-Mail</label>
                    <input type="text" class="form-control" name="email" value="{{old('email')??Auth::user()->email}}" disabled>
                    @error('email') <span class="text-danger">{{$message}}</span> @enderror
                </div>
                <div class="form-group">
                    <label for="">Alamat</label>
                    <textarea name="alamat" class="form-control @error('alamat') is-invalid @enderror" id="" cols="10" rows="5">{{old('alamat')??$profil->alamat}}</textarea>
                    @error('alamat') <span class="text-danger">{{$message}}</span> @enderror
                </div>

                <div class="form-group">
                    <label for="">No Telp</label>
                    <input type="number" name="telp" value="{{old('telp')??$profil->no_telp}}" class="form-control @error('telp') is-invalid @enderror">
                    @error('telp') <span class="text-danger">{{$message}}</span> @enderror
                </div>

                <div class="form-group">
                    <label for="">Tempat Lahir</label>
                    <input type="text" class="form-control @error('tempat_lahir') is-invalid @enderror" name="tempat_lahir" value="{{old('tempat_lahir')??$profil->tempat_lahir}}">
                    @error('tempat_lahir') <span class="text-danger">{{$message}}</span> @enderror
                </div>

                <div class="form-group">
                    <label for="">Tanggal Lahir</label>
                    <input type="date" class="form-control @error('tanggal_lahir') is-invalid @enderror" name="tanggal_lahir" value="{{old('tanggal_lahir')??$profil->tanggal_lahir}}">
                    @error('tanggal_lahir') <span class="text-danger">{{$message}}</span> @enderror
                </div>

                <label for="" class="">Jenis Kelamin</label>
                <div class="form-check">
                    <input type="radio" name="jenis_kelamin" id="" class="form-check-input" value="L" {{(old('jenis_kelamin')??$profil->jenis_kelamin)=='L'?'checked':''}}><label for="" class="form-check-label">Pria</label>
                </div>
                <div class="form-check">
                    <input type="radio" name="jenis_kelamin" id="" class="form-check-input" value="P" {{(old('jenis_kelamin')??$profil->jenis_kelamin)=='P'?'checked':''}}><label for="" class="form-check-label">Wanita</label>
                </div>
                @error('jenis_kelamin') <span class="text-danger">{{$message}}</span> @enderror

        </div>
        <div class="card-footer"><button type="submit" class="btn btn-primary">Update</button>
        </div>
        </form>
    </div>
@endsection
