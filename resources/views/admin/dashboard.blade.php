@extends('layouts.admin-lte.master')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-12">
                <div class="card bg-blue">
                    <div class="card-body">
                        <div class="row text-white">
                            <div class="col"><i class="fas fa-user-cog fa-5x "></i></div>
                            <div class="col"><h4 class="font-weight-bold">Edit Profile</h4>
                                <a href="{{route('profil.edit',['id'=>Auth::id()])}}" class="btn btn-light">Edit</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-12">
                <div class="card bg-warning">
                    <div class="card-body">
                        <div class="row text-white">
                            <div class="col"><i class="fas fa-newspaper fa-5x"></i></div>
                            <div class="col"><h4 class="font-weight-bold">Postingan</h4>
                                <a href="{{route('post.index')}}" class="btn btn-light">Lihat</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('script')

    @if(session('notif') == 'updated')
        <script !src="">
            swal({
                title: "Berhasil!",
                text: "Data Berhasil Diupdate",
                icon: "success",
                button: "Ok",
            });</script>
        @php
        session()->forget('notif');
        @endphp
    @endif
@endpush
