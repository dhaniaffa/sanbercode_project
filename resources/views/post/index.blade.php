@extends('layouts.admin-lte.master')
@section('content')


<section class="content">
    <div class="container-fluid">
        <a href="/post/create" class="btn btn-primary mb-3">Buat Postingan</a>
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card">
                            <div class="card-header">
                                <h3 class="card-title"><b>Postingan</b></h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <table id="example1" class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Judul</th>
                                    <th>Tanggal Diposting</th>
                                    <th>Aksi</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($post as $item)
                                    <tr>
                                        <td>{{$loop->iteration}}</td>
                                        <td>{{str::words($item->judul,6)}}</td>
                                        <td>{{date('d-m-Y',strtotime($item->created_at))}}</td>
                                        <td class="d-md-inline-block">
                                            <a href="/post/{{$item->id}}/edit" class="btn btn-warning">Edit</a>
                                            <a href="{{route('front.show',['judul'=>str::slug($item->judul,'-'),'id'=>$item->id])}}" class="btn btn-info">Lihat</a>
                                            <div class="d-md-inline-block">
                                                <form action="/post/{{$item->id}}" method="post" class="">
                                                    @csrf
                                                    @method('DELETE')
                                                    <button type="submit" id="modalButton" class="btn btn-danger" onclick="return confirm('Apakah yakin akan dihapus?')" data-toggle="modal" data-target="#modalHapus" data-id="">Hapus</a>
                                                </form>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                        <!-- /.card-body -->

                        <!-- Modal -->
                       {{-- <div class="modal fade" id="modalHapus" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel">Peringatan!!!</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        Apakah anda yakin ingin menghapus data ini?
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Tidak</button>
                                        <form action="" id="deleteForm" method="post">
                                            @csrf
                                            @method('delete')
                                            <button type="submit" class="btn btn-danger">Ya</button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>--}}
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

@push('script')

    @if(session('pesan') !='')
        <script !src="">
            swal({
                title: "Berhasil!",
                text: "Artikel Berhasil {{session('pesan')}}",
                icon: "success",
                button: "Ok",
            });</script>
        @php
            session()->forget('notif');
        @endphp
    @endif
@endpush
