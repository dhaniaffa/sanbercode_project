<!DOCTYPE html>
<html class="no-js" lang="en">
@include('frontend.assets.head')

<body>

@include('frontend.assets.preloader')
<div id="top" class="s-wrap site-wrapper">
    @include('frontend.assets.samping')
    @yield('konten')


    <!-- footer
    ================================================== -->
    <footer class="s-footer">
        <div class="row">
            <div class="column large-full footer__content">
                <div class="footer__copyright">
                    <span>© Copyright Typerite 2019</span>
                    <span>Design by <a href="https://www.styleshout.com/">StyleShout</a></span>
                </div>
            </div>
        </div>

        <div class="go-top">
            <a class="smoothscroll" title="Back to Top" href="#top"></a>
        </div>
    </footer>

</div> <!-- end s-wrap -->


<!-- Java Script
================================================== -->
<script src="{{asset('/assets/typerite-master/js/jquery-3.2.1.min.js')}}"></script>
<script src="{{asset('/assets/typerite-master/js/plugins.js')}}"></script>
<script src="{{asset('/assets/typerite-master/js/main.js')}}"></script>

</body>

</body>
