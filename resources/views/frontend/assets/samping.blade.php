<!-- site header
    ================================================== -->
<header class="s-header">

    <div class="header__top">
{{--        <div class="header__logo">
            <a class="site-logo" href="{{route('home')}}">
                <i class="fas fa-home text-white fa-4x"></i>
            </a>
        </div>--}}
        <a href="#0" class="header__menu-toggle"><span>Menu</span></a>

    </div> <!-- end header__top -->

    <nav class="header__nav-wrap">

        <ul class="header__nav">
            <li class="current"><a href="{{route('home')}}" title="">Home</a></li>
            @if(Route::has('login'))
            @auth()
            <li>
                <a href="{{route('profil.edit',['id' => Auth::id()])}}" title="">Profil Saya</a>
                <a href="{{route('dashboard')}}">Dashboard</a>
                <a href="" onclick="event.preventDefault(); document.getElementById('logout-front').submit();">Keluar</a>
                <form id="logout-front" action="{{ route('logout') }}" method="POST" class="d-none">
                    @csrf
                </form>
            </li>

            @else
            <li class="has-children">
                <a href="" title="">Login</a>
                <ul class="sub-menu">
                    <li><a href="{{route('login')}}">Log In</a></li>
                    <li><a href="{{route('register')}}">Daftar Akun</a></li>
                </ul>
            </li>
            @endauth
            @endif

        </ul> <!-- end header__nav -->

    </nav> <!-- end header__nav-wrap -->



</header> <!-- end s-header -->
