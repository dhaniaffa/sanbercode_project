<head>

    <!--- basic page needs
    ================================================== -->
    <meta charset="utf-8">
    <title>Blog Sederhana Kami</title>
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- mobile specific metas
    ================================================== -->
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSS
    ================================================== -->
    <link rel="stylesheet" href="{{asset('/assets/typerite-master/css/base.css')}}">
    <link rel="stylesheet" href="{{asset('/assets/typerite-master/css/vendor.css')}}">
    <link rel="stylesheet" href="{{asset('/assets/typerite-master/css/main.css')}}">

    <!-- script
    ================================================== -->
    <script src="{{asset('/assets/typerite-master/js/modernizr.js')}}"></script>
    <script src="https://kit.fontawesome.com/2735e24c5d.js" crossorigin="anonymous"></script>

    <!-- favicons
    ================================================== -->
    <link rel="apple-touch-icon" sizes="180x180" href="apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="{{asset('/assets/typerite-master/images/16.png')}}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{asset('/assets/typerite-master/images/32.png')}}">
    <link rel="manifest" href="site.webmanifest">

</head>
