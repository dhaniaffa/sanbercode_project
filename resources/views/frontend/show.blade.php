@extends('frontend.assets.master')
@section('konten')
    <div class="s-content content">
        <main class="row content__page">

            <article class="column large-full entry format-standard">
                <div class="content__page-header entry__header">
                    <h1 class="display-1 entry__title">
                        {{$artikel->judul}}
                    </h1>
                    <ul class="entry__header-meta">
                        <li class="author">Oleh <a href="#0">{{$artikel->user->name}}</a></li>
                        <li class="date">{{date('d M Y',strtotime($artikel->created_at))}}</li>
                    </ul>
                </div> <!-- end entry__header -->

                <div class="entry__content">
                    <div class="drop-cap">
                        {!! $artikel->isi !!}
                    </div>
                </div> <!-- end entry content -->

                <div class="entry__related">
                    <h3 class="h2">Baca Juga</h3>

                    <ul class="related">
                        @foreach($random as $rand)
                            <article class="masonry__brick entry format-standard animate-this">
                                <div class="entry__text">
                                    <div class="entry__header">
                                        <h2 class="entry__title"><a href="{{route('front.show',['judul'=>str::slug($rand->judul,'-'),'id'=>$rand->id])}}">{{$rand->judul}}</a></h2>
                                    </div>
                                </div>

                            </article>
                        @endforeach
                    </ul>
                </div> <!-- end entry related -->

            </article> <!-- end column large-full entry-->

            <div class="comments-wrap">

                <div id="comments" class="column large-12">

                    @foreach($jumlah as $jml)
                        <h3 class="h2">{{$jml->comments_count}} Komentar</h3>
                    @endforeach

                    <!-- START commentlist -->
                    <ol class="commentlist">

                        <li class="depth-1 comment">

                            @foreach($artikel->comments as $komen)
                                <div class="comment__content">
                                    @php
                                        $user = \App\Models\Post::where('user_id',$komen->user_id)->first();
                                        echo "<div class='comment__info'>
                                        <div class='comment__author'>{$user->user->name}</div>";
                                    @endphp


                                        <div class="comment__meta">
                                            <div class="comment__time">{{$komen->created_at}}</div>
                                            <div class="comment__reply">
                                                <a class="comment-reply-link" href="#0">Reply</a>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="comment__text">
                                        <p>{{$komen->komentar}}</p>
                                    </div>

                                </div>
                            @endforeach

                        </li> <!-- end comment level 1 -->
                    </ol>
                    <!-- END commentlist -->

                </div> <!-- end comments -->

                <div class="column large-12 comment-respond">

                    <!-- START respond -->
                    <div id="respond">

                        <h3 class="h2">Beri Komentar </h3>
                        <form name="contactForm" id="contactForm" action="{{route('komen.store',['judul'=>str::slug($artikel->judul,'-'),'id'=>$artikel->id])}}"  method="post">
                           @csrf

                            <fieldset>
                                <div class="message form-field">
                                    <textarea name="komentar" id="cMessage" class="full-width" placeholder="Komentar"></textarea>
                                </div>

                                <button class="btn btn--primary btn-wide btn--large full-width" value="Add Comment" type="submit">Beri Komentar</button>

                            </fieldset>
                        </form> <!-- end form -->



                    </div>
                    <!-- END respond-->

                </div> <!-- end comment-respond -->

                </div> <!-- end comments-wrap -->
                </main>

                </div> <!-- end s-content -->
                @endsection
