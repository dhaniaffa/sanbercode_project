@extends('frontend.assets.master')
@section('konten')
    <div class="s-content">

        <div class="masonry-wrap">

            <div class="masonry">

                <div class="grid-sizer"></div>
                @forelse($posts as $post)

                    <article class="masonry__brick entry format-standard animate-this">

                        <div class="entry__text">
                            <div class="entry__header">
                                <h2 class="entry__title"><a href="{{route('front.show',['judul'=>str::slug($post->judul,'-'),'id'=>$post->id])}}">{{$post->judul}}</a></h2>
                                <div class="entry__meta">
                        <span class="entry__meta-cat">
                            <a href="category.html">{{str::limit($post->user->name,10)}}</a>
                        </span>
                                    <span class="entry__meta-date">
                            <a href="single-standard.html">{{date('d M Y',strtotime($post->created_at))}}</a>
                        </span>
                                </div>
                            </div>
                            <div class="entry__excerpt">
                                <p>
                                    {!! str::limit($post->isi, 80) !!}
                                </p>
                            </div>
                        </div>

                    </article>
                @empty
                    <div class="content__page-header entry__header">
                        <h1 class="display-1 entry__title">
                            Belum Ada Postingan, Yuk Posting!!!!!
                        </h1>
                    </div>
                @endforelse
            </div> <!-- end masonry -->

        </div> <!-- end masonry-wrap -->
{{--
        <div class="row">
            <div class="column large-full">
                <nav class="pgn">
                    <ul>
                        <li><a class="pgn__prev" href="#0">Prev</a></li>
                        <li><a class="pgn__num" href="#0">1</a></li>
                        <li><span class="pgn__num current">2</span></li>
                        <li><a class="pgn__num" href="#0">3</a></li>
                        <li><a class="pgn__num" href="#0">4</a></li>
                        <li><a class="pgn__num" href="#0">5</a></li>
                        <li><span class="pgn__num dots">…</span></li>
                        <li><a class="pgn__num" href="#0">8</a></li>
                        <li><a class="pgn__next" href="#0">Next</a></li>
                    </ul>
                </nav>
            </div>
        </div>--}}

    </div> <!-- end s-content -->

@endsection
