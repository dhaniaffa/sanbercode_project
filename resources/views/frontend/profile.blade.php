@extends('frontend.assets.master')
@section('konten')
    <div class="card" >
        <div class="card-header text-white bg-dark" style="font-size: 25px">
            <b >Profil "{{$user->name}}"</b>
        </div>
        <div class="card-body "style="font-size: 20px">
            <div class="card-text">
                <table class="table table-hover table-bordered">
                    <tr>
                        <td>Alamat</td>
                        <td>{{$user->profiluser->alamat}}</td>
                    </tr>
                    <tr>
                        <td>No. Telepon</td>
                        <td>{{$user->profiluser->no_telp}}</td>
                    </tr>
                    <tr>
                        <td>Tempat Lahir</td>
                        <td>{{$user->profiluser->tempat_lahir}}</td>
                    </tr>
                    <tr>
                        <td>Tanggal Lahir</td>
                        <td>{{$user->profiluser->tanggal_lahir}}</td>
                    </tr>
                    <tr>
                        <td>Jenis Kelamin</td>
                        <td>{{$user->profiluser->jenis_kelamin}}</td>
                    </tr>
                </table>
            </div>
        </div>
        <div class="card-footer" style="font-size: 10px">
            <a href="{{route('dashboard')}}"><i class="fas fa-tachometer-alt fa-2x"> Dashboard</i></a>
        </div>
    </div>
@endsection
