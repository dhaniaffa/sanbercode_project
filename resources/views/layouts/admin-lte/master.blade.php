@include('layouts.admin-lte.parent.head')
<body class="hold-transition sidebar-mini">
<div class="wrapper">
@include('layouts.admin-lte.parent.navbar')

@include('layouts.admin-lte.parent.aside')
@stack('scripts-head')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>{{$data??'Dashboard'}}</h1>
          </div>
      </div>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
            <div class="col-12">
            @yield('content')
            </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

@include('layouts.admin-lte.parent.footer')
</div>
<!-- ./wrapper -->

@include('layouts.admin-lte.parent.script')
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

@stack('script')

</body>
</html>
