  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="{{route('dashboard')}}" class="brand-link">
      <img src="{{asset('/icon/gear.png')}}"
           alt="AdminLTE Logo"
           class="brand-image img-circle elevation-3"
           style="opacity: .8">
      <span class="brand-text font-weight-light">Dashboard Blog</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="{{asset('/icon/avatar.png')}}" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          <a href="#" class="d-block">{{str::limit(Auth::user()->name,20)}}</a>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          <li class="nav-item has-treeview">
            <a href="{{route('dashboard')}}" class="nav-link {{$statusDashboard??''}}">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p> Dashboard</p>
            </a>
          </li>
            <li class="nav-item has-treeview">
                <a href="{{route('home')}}" class="nav-link">
                    <i class="fas fa-blog nav-icon"></i>
                    <p>Blog</p>
                </a>
            </li>
            <li class="nav-item has-treeview">
                <a href="{{route('post.index')}}" class="nav-link {{$statusPosting??''}}">
                    <i class="nav-icon fas fa-newspaper"></i>
                    <p>Postingan</p>
                </a>
            </li>
            <li class="nav-item">
                <a href="#" class="nav-link {{$status??''}}">
                    <i class="nav-icon fas fa-sign-out-alt"></i>
                    <p onclick="event.preventDefault(); document.getElementById('logout-link').submit();">Keluar</p>
                    <form id="logout-link" action="{{ route('logout') }}" method="POST" class="d-none">
                        @csrf
                    </form>
                </a>
            </li>

        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>
