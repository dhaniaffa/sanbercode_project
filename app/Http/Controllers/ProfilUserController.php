<?php

namespace App\Http\Controllers;

use App\Models\ProfilUser;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;


class ProfilUserController extends Controller
{
    protected $casts = [
        'tanggal_lahir' => 'date',
    ];

    public function edit($id){
        if ($id == Auth::id()){
            $profil = ProfilUser::where('user_id',$id)->first();
            if ($profil == null){
                return view('admin.edit-user-profil',['data' => 'Edit Profil']);
            } else {
                return view('admin.update-user-profil',['profil' => $profil,'data' => 'Edit Profil']);
            }
        } else{
            return redirect()->route('dashboard');
        }
    }

    public function store(Request $request,$id){
        $rules = [
            'name' => 'required|min:3',
            'alamat' => 'nullable|min:5',
            'telp' => 'nullable|numeric|min:11',
            'tempat_lahit' => 'nullable',
            'tanggal_lahir' => 'nullable|date',
            'jenis_kelamin' => 'nullable',
        ];

        $pesan = [
          'required' => 'Kolom ini wajib diisi!',
          'name.min' => 'Masukan nama anda minimal 3 karakter!',
          'telp.numeric' => 'Data yang dimasukkan haruslah angka!',
          'telp.min' => 'Minimal 11 Karakter!',
          'date' => 'Data harus lah berbentuk tanggal! contoh: Bulan - Hari - Tahun',
        ];
        $data = Validator::make($request->all(),$rules,$pesan);

        if ($data->fails()){
            return redirect()->route('profil.edit',['id'=>$id])->withErrors($data)->withInput();
        } else {
            $user = User::where('id',$id)->update([
               'name' => $request->name,
                'updated_at' => now(),
            ]);

            $userId = User::where('id',$id)->first();
            $userId->profiluser()->create([
               'alamat' => $request->alamat,
               'no_telp' => $request->telp,
                'tempat_lahir' => $request->tempat_lahir,
                'tanggal_lahir' => $request->tanggal_lahir,
                'jenis_kelamin' => $request->jenis_kelamin,
            ]);

            return redirect()->route('dashboard')->with('notif','updated');
        }
    }

    public function update(Request $request, $id){
        $rules = [
            'name' => 'required|min:3',
            'alamat' => 'nullable|min:5',
            'telp' => 'nullable|numeric|min:11',
            'tempat_lahit' => 'nullable',
            'tanggal_lahir' => 'nullable|date',
            'jenis_kelamin' => 'nullable',
        ];

        $pesan = [
            'required' => 'Kolom ini wajib diisi!',
            'name.min' => 'Masukan nama anda minimal 3 karakter!',
            'telp.numeric' => 'Data yang dimasukkan haruslah angka!',
            'telp.min' => 'Minimal 11 Karakter!',
            'date' => 'Data harus lah berbentuk tanggal! contoh: Bulan - Hari - Tahun',
        ];
        $data = Validator::make($request->all(),$rules,$pesan);

        if ($data->fails()){
            return redirect()->route('profil.edit',['id'=>$id])->withErrors($data)->withInput();
        } else {
            $user = User::where('id',$id)->update([
                'name' => $request->name,
                'updated_at' => now(),
            ]);

            $userId = User::find($id);
            $userId->profiluser()->update([
                'alamat' => $request->alamat,
                'no_telp' => $request->telp,
                'tempat_lahir' => $request->tempat_lahir,
                'tanggal_lahir' => $request->tanggal_lahir,
                'jenis_kelamin' => $request->jenis_kelamin,
            ]);

            return redirect()->route('dashboard')->with('notif','updated');
        }
    }
}
