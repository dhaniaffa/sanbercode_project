<?php

namespace App\Http\Controllers;

use App\Models\Comment;
use App\Models\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;



class CommentController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }


    public function store(Request $request,$judul,$id)
    {
        $artikel = Post::where('id',$id)->first();
        $komentar = new Comment();
        $komentar->komentar = $request->komentar;
        $komentar->user_id = Auth::id();
        $komentar->save();
        $artikel->comments()->attach($komentar);


        return redirect()->back();
    }

}
