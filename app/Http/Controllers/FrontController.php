<?php

namespace App\Http\Controllers;

use App\Models\Comment;
use App\Models\Post;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class FrontController extends Controller
{
    public function home(){
        $posts = Post::all();
        return view('frontend.home',['posts' => $posts]);
    }

    public function show($judul,$id){
        $artikel = Post::where('id',$id)->first();
        $jml = Post::where('id',$id)->withCount('comments')->get();
        $komentar = Comment::where('user_id',Auth::id())->get();
        $random = Post::inRandomOrder()->limit(3)->get();
/*        foreach ($artikel->comments as $komen){
            $user = Post::where('user_id',$komen->user_id)->first();
            echo $komen->komentar." komentar ini oleh : ".$user->user->name."<br>";

        }*/
    return view('frontend.show',['artikel' =>$artikel,'random' => $random,'koment'=>$artikel,'jumlah' => $jml]);
    }

    public function profil($id){
        $user = User::where('id',$id)->first();
        return view('frontend.profile',['user' => $user]);
    }
}
