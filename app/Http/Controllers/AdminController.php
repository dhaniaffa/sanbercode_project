<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AdminController extends Controller
{
    public function index(){
        $status = 'active';
        return view('admin.dashboard',['statusDashboard' => $status,'data' => 'Dashboard']);
    }
}
