<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProfilUser extends Model
{
    use HasFactory;
    protected $guarded = [];
    public function user(){
        return $this->belongsTo('App\Models\User');
    }
/*    public function getTanggalLahirAttribute($value){
        $nama_bulan = ["Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November","Desember"];
        $tanggal = date('j',strtotime($value));
        $bulan = date('n',strtotime($value))-1;
        $tahun = date('Y',strtotime($value));
        return $tanggal." ".$nama_bulan[$bulan]." ".$tahun;
    }*/

/*    public function getJenisKelaminAttribute($value){
        if ($value == 'L'){
            return 'Laki - Laki';
        } elseif ($value == 'P'){
            return 'Perempuan';
        }
    }*/
}
