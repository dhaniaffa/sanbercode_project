<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $table = "posts";
    protected $fillable = ["judul", "isi"];
    use HasFactory;
    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    public function comments()
    {
        return $this->belongsToMany('App\Models\Comment');
    }

    public function user2(){
        return $this->hasOneThrough('App\Models\User','App\Models\Comment');
    }
}
