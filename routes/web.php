<?php

use Illuminate\Support\Facades\Route;
use \App\Http\Controllers\ProfilUserController;
use App\Http\Controllers\AdminController;
use App\Http\Controllers\FrontController;
use App\Http\Controllers\CommentController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [FrontController::class,'home'])->name('home');

Route::middleware('auth')->group(function () {
    Route::get('/artikel/{judul}/{id}',[FrontController::class,'show'])->name('front.show');

    Route::get('/dashboard', [AdminController::class, 'index'])->name('dashboard');

    Route::get('/dashboard/user/{id}/profile/edit', [ProfilUserController::class, 'edit'])->name('profil.edit')->middleware();

    Route::post('/dashboard/user/{id}/profile/edit', [ProfilUserController::class, 'store'])->name('profil.store');

    Route::put('/dashboard/user/{id}/profile/edit', [ProfilUserController::class, 'update'])->name('profil.update');
    Route::post('/artikel/{judul}/{id}',[CommentController::class,'store'])->name('komen.store');

/*    Route::get('/comments',[CommentController::class,'index'])->name('comments.index');
    Route::get('/comments/create',[CommentController::class,'showCreateForm'])->name('comments.create');*/



});



Route::get('/coba', function () {
   return view('layouts.admin-lte.master');
});

Auth::routes();
Route::get('/profil/user/edit', [ProfilUserController::class, 'edit']);
/*Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');*/
Route::group(['prefix' => 'laravel-filemanager', 'middleware' => ['web', 'auth']], function () {
    \UniSharp\LaravelFilemanager\Lfm::routes();
});

Route::resource('/post', 'PostController');



